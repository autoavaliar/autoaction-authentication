<?php
declare(strict_types=1);

namespace AutoAction\Authentication;

use App\Core\Common\Enum\AuthTypeTokenEnum;
use Firebase\JWT\JWT;

/**
 * Class EncodeJwtToken
 * @package Autoaction\Authentication
 * @date    09/10/2018
 * @author Hugo Leonardo <hugo.santos@autoavaliar.com.br>
 */
class EncodeJwtToken
{
    /**
     * @var mixed
     */
    private $key;
    /**
     * @var mixed
     */
    private $hash;
    /**
     * @var array
     */
    private $data;
    /**
     * @var mixed
     */
    private $authTTL;
    /**
     * @var mixed
     */
    private $refreshTTL;
    /**
     * @var string
     */
    private $typeToken;

    /**
     * EncodeJwtToken constructor.
     * @param array $config
     * @param string $typeToken
     */
    function __construct(array $config, $typeToken = AuthTypeTokenEnum::ACCESS)
    {
        $this->key = $config['key'];
        $this->iss = $config['iss'];
        $this->hash = $config['hash'];
        $this->authTTL = $config['authTTL'];
        $this->refreshTTL = $config['refreshTTL'];
        $this->typeToken = $typeToken;
    }

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }

    /**
     * @param $typeToken
     */
    public function setType($typeToken)
    {
        $this->typeToken = $typeToken;
    }

    /**
     * @return string
     */
    private function encode()
    {
        $dueDate = ($this->typeToken == 'access_token') ? $this->authTTL : $this->refreshTTL;
        $dueDate += time();

        return JWT::encode(
            [
                'iss' => $this->iss, // emissor do token
                'iat' => time(), // hora que o token foi gerado
                'exp' => $dueDate, // expiracao do token
                'data' => $this->data // dados armazenados no token
            ], $this->key, $this->hash);
    }

    /**
     * @return string
     */
    public function execute()
    {
        return $this->encode();
    }
}