<?php

namespace AutoAction\Authentication;
use Firebase\JWT\JWT;
/**
 * Class decodePayloadJwtToken
 * @package Autoaction\Authentication
 * @date    09/10/2018
 * @author Hugo Leonardo <hugo.santos@autoavaliar.com.br>
 */
class DecodeJwtToken
{
    private $key;
    private $token;
    private $hash;

    /**
     * decodePayloadJwtToken constructor.
     * @param $token
     * @param $config
     */
    function __construct($token, $config)
    {
        $this->token = $token;
        $this->key = $config["key"];
        $this->hash = $config["hash"];
    }

    /**
     * @return object
     */
    public function execute()
    {
        return JWT::decode($this->token, $this->key, array($this->hash));
    }
}